import react, { useState, useEffect } from 'react'

import { Button, TextField, FormControl, InputLabel, Input, FormHelperText } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';

/* Component to take an employee input */
const EmployeeInput = (props) => {

    // States for storing employee information
    const [employeeId, setEmployeeId] = useState('');
    const [employeeName, setEmployeeName] = useState('');
    const [employeeType, setEmployeeType] = useState('');
    const [phones, setPhones] = useState([]);
    
    // Other states
    const [errorMessage, setErrorMessage] = useState(null);

    const idChanged = (event) => {
        setEmployeeId(event.target.value);
    }

    const nameChanged = (event) => {
        setEmployeeName(event.target.value);
    }

    const typeChanged = (event) => {
        setEmployeeType(event.target.value);
    }

    const phoneChanged = (event) => {
        const index = event.target.dataset.index;
        console.debug(index, "is the index", event.target.dataset.index)
        const currentPhones = [...phones]
        currentPhones[index] = event.target.value
        setPhones(currentPhones);
    }

    const addPhoneClick = () => {
        setPhones([...phones, '']);
    }

    const submitClick = () => {
        // Some validations
        if(props.idRequired && employeeId.length == 0){
            setErrorMessage("ID can't be empty");
            return;
        }
        if(employeeName.length == 0){
            setErrorMessage("Name can't be empty");
            return;
        }
        if(employeeType.length == 0){
            setErrorMessage("Type can't be empty");
            return;
        }
        for(let i=0; i<phones.length; i++){
            console.log(phones[i].length);
            if(phones[i].length == 0){
                setErrorMessage("One more more phone numbers are empty");
                return;
            }
        }

        setErrorMessage(null);

        // Create the JSON object for employee
        const employeeData = {
            name: employeeName,
            type: employeeType,
            phones: phones
        }
        if(employeeId.length > 0)
            employeeData.eid = employeeId;

        console.debug("Employee object is ", employeeData);
        props.onSubmit(employeeData);

        /* Clear the input fields */
        setEmployeeId("");
        setEmployeeName("");
        setEmployeeType("");
        setPhones([]);
        
    }

    return (
        <div>
            <FormControl>
                <TextField 
                    label="ID" 
                    value={employeeId} 
                    onChange={idChanged}
                    required={props.idRequired? true: false}
                />
                <TextField label="Name" required value={employeeName} onChange={nameChanged}/>
                <TextField label="Type" required value={employeeType} onChange={typeChanged}/> 
                
                {phones.map((phone, i) => {
                    return (
                        <TextField 
                            key={i} 
                            label={"Phone number " + (i+1)}
                            value={phones[i]}
                            inputProps={{
                                'data-index': i
                            }}
                            onChange={phoneChanged} 
                            required
                        />
                    )
                })}

                <br />
                <Button onClick={addPhoneClick} variant="outlined" size="small">
                    add phone
                </Button>
                <br />

                {errorMessage &&
                    <Alert severity="warning">
                        {errorMessage}                
                    </Alert>
                }
                <br />

                <Button onClick={submitClick} variant="outlined">
                    {props.buttonText}
                </Button>      
            </FormControl>
        </div>
    )
}

export default EmployeeInput;