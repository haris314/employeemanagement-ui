import react from 'react'
import { Button } from '@material-ui/core'
import { Link } from 'react-router-dom'


const NavigationBarButton = (props) =>{

    return(
        
        <Button 
            component={ Link } 
            to={props.url}>
            {props.text}
        </Button>
        
    )
}

export default NavigationBarButton;