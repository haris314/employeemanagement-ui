import react from "react"
import {AppBar, Toolbar, Button, Typography, Grid} from "@material-ui/core"
import NavigationBarButton from "./NavigationBarButton.js"

const NavigationBar = (props) =>{

    return(
        <div>
            <AppBar position="static">
                <Toolbar>
                    <Grid justify="space-between" container spacing={10}>
                        <Grid item>
                            <Typography>Employee Management</Typography>
                        </Grid>
                        <Grid item>
                            <NavigationBarButton url="/view" text="View" />
                            <NavigationBarButton url="/register" text="Register" />
                            <NavigationBarButton url="/update" text="Update" />
                            <NavigationBarButton url="/delete" text="Delete" />
                            <NavigationBarButton url="/search" text="Search" />
                        </Grid>
                    </Grid>
                </Toolbar>
           </AppBar>
        </div>
    )
}

export default NavigationBar
