import react from 'react'

/* Component to display the details of one Employee */

const style = {
    border: "1px solid black",
    textAlign: "left",
    margin: "20px 10% 20px 10%",
    padding: "20px",
    boxShadow: "1px 1px 10px grey",
}

const phoneStyle = {
    fontSize: "12px",
    color: "black",
    padding: "2px",
    margin: "2px",
    border: "1px solid black"
}

const Employee = (props) => {

    return (
        <div style={style}> 
            <table>
                <tbody>
                    <tr>
                        <td><b>Employee ID: </b></td>
                        <td>{props.employee.eid} </td>
                    </tr>
                    <tr> 
                        <td> <b>Name:</b> </td>
                        <td> {props.employee.name} </td>
                    </tr>
                    <tr> 
                        <td> <b>Type:</b> </td>
                        <td> {props.employee.type} </td>
                    </tr>
                    {props.employee.phones && 
                        <tr> 
                            <td> <b>Phone numbers:</b>  </td>
                            <td>
                                {props.employee.phones.map((phone, i) => {
                                    return <div key={i} style={phoneStyle}>{phone}</div>
                                })}
                            </td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Employee;