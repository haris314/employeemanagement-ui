import { REST_ENDPOINT } from "../Constants"
import axios from 'axios'

const EmployeeApi = {

    getEmployee: async (employeeId) => {
        return axios.get(`${REST_ENDPOINT}/${employeeId}`).then((res) => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log("Got error", error)
            throw Error(error.response? error.response.data.message : "Something went wrong");
        });         
    },

    addEmployee: async (employeeData) => {
        return axios.post(REST_ENDPOINT, employeeData).then((res) => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log("Got error", error)
            throw Error(error.response? error.response.data.message : "Something went wrong");
        });         
    },

    updateEmployee: async (employeeData) => {
        return axios.put(`${REST_ENDPOINT}/${employeeData.eid}`, employeeData).then((res) => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log("Got error", error)
            throw Error(error.response? error.response.data.message : "Something went wrong");
        });         
    },

    deleteEmployee: async (employeeId) => {
        return axios.delete(`${REST_ENDPOINT}/${employeeId}`).then((res) => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log("Got error", error)
            throw Error(error.response? error.response.data.message : "Something went wrong");
        });         
    },

    searchEmployees: async (searchData) => {
        return axios.post(`${REST_ENDPOINT}/search`, searchData).then((res) => {
            console.log(res);
            return res.data;
        })
        .catch(error => {
            console.log("Got error", error)
            throw Error(error.response? error.response.data.message : "Something went wrong");
        });         
    },
}

export default EmployeeApi;