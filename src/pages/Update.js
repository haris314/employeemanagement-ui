import react, { useState } from 'react'

import { Alert } from '@material-ui/lab'

import EmployeeInput from '../components/EmployeeInput'
import EmployeeApi from "../APIs/EmployeeApi"

const Update = (props) =>{

    const [errorMessage, setErrorMessage] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);

    const updateEmployee = (employeeData) => {
        EmployeeApi.updateEmployee(employeeData).then((res) => {
            console.log("Got response", res);
            setErrorMessage(null);
            setSuccessMessage(`${res.name}(ID: ${res.eid}) has been successfully updated`);
        })
        .catch(error => {
            console.log(error.message);
            setErrorMessage(error.message);
            setSuccessMessage(null);
        })
    }

    return (
        <div>
            <h2>Update an Employee</h2>
            <EmployeeInput 
                buttonText="Update" 
                onSubmit={updateEmployee}
                idRequired={true}
            />
            <br />
            {errorMessage && 
                <Alert severity="error">
                    {errorMessage}
                </Alert>    
            }
            {successMessage &&
                <Alert severity="success">
                    {successMessage}
                </Alert>
            }
            
        </div>
    )
}

export default Update;