import react, { useState } from 'react'

import { Alert } from '@material-ui/lab'

import EmployeeInput from '../components/EmployeeInput'
import EmployeeApi from "../APIs/EmployeeApi"

const Register = (props) =>{

    const [errorMessage, setErrorMessage] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);

    const registerEmployee = (employeeData) => {
        EmployeeApi.addEmployee(employeeData).then((res) => {
            console.log("Got response", res);
            setErrorMessage(null);
            setSuccessMessage(`${res.name}(ID: ${res.eid}) has been successfully added`);
        })
        .catch(error => {
            console.log(error.message);
            setErrorMessage(error.message);
            setSuccessMessage(null);
        })
    }

    return (
        <div>
            <h2>Register a new Employee</h2>
            <EmployeeInput 
                buttonText="Register" 
                onSubmit={registerEmployee}
                idRequired={false}
            />
            <br />
            {errorMessage && 
                <Alert severity="error">
                    {errorMessage}
                </Alert>    
            }
            {successMessage &&
                <Alert severity="success">
                    {successMessage}
                </Alert>
            }

        </div>
    )
}

export default Register;