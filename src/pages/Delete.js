import react, { useState } from 'react'
import { TextField, Input, Button } from "@material-ui/core"
import { Alert } from "@material-ui/lab"

import Employee from "../components/Employee"
import EmployeeApi from "../APIs/EmployeeApi"

const Delete = (props) => {

    const [employeeId, setEmployeeId] = useState('');
    const [errorMessage, setErrorMessage] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);

    const deleteEmployeeClick = () => {

        EmployeeApi.deleteEmployee(employeeId).then((res) => {
                console.log("Got response", res);
                setErrorMessage(null);
                setSuccessMessage(res.message);
            })
            .catch(error => {
                console.log(error.message);
                setErrorMessage(error.message);
                setSuccessMessage(null);
            })

    }

    const employeeIdChanged = (event) => {
        setEmployeeId(event.target.value);
    }

    return (
        <div>
            <h2>Delete an existing Employee</h2>
            <TextField required value={employeeId} onChange={employeeIdChanged} label="Employee ID" />
            <br /> <br />
            <Button
                variant="outlined"
                onClick={deleteEmployeeClick}>
                Delete
            </Button>

            <br /> < br />
            {errorMessage &&
                <Alert severity="error">
                    {errorMessage}                
                </Alert>
            }
            {successMessage &&
                <Alert severiy="success">
                    {successMessage}
                </Alert>
            }
        </div>
    )
}

export default Delete;