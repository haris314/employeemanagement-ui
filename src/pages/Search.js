import react, { useState } from 'react'

import { TextField, FormControl, Button } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

import Employee from '../components/Employee'
import EmployeeApi from '../APIs/EmployeeApi'

const Search = (props) =>{

    /* State Variables to store Search criteria */
    const [employeeId, setEmployeeId] = useState('');
    const [employeeName, setEmployeeName] = useState('');
    const [employeeType, setEmployeeType] = useState('');
    const [employeePhone, setEmployeePhone] = useState('');

    /* State Variables for Search results */
    const [errorMessage, setErrorMessage] = useState(null);
    const [searchResults, setSearchResults] = useState([]);

    const employeeIdChanged = (event) => {
        setEmployeeId(event.target.value)
    }

    const employeeNameChanged = (event) => {
        setEmployeeName(event.target.value);
    }

    const employeeTypeChanged = (event) => {
        setEmployeeType(event.target.value);
    }

    const employeePhoneChanged = (event) => {
        setEmployeePhone(event.target.value);
    }

    const searchClick = () => {
        // Get the search data
        const searchData = {}
        if(employeeId.length > 0){
            searchData.eid = employeeId;
        }
        if(employeeName.length > 0){
            searchData.name = employeeName;
        }
        if(employeeType.length > 0){
            searchData.type = employeeType;
        }
        if(employeePhone.length > 0){
            searchData.phone = employeePhone;
        }

        // Get the search results
        EmployeeApi.searchEmployees(searchData).then((res) => {
            console.log("Got response", res);
            setErrorMessage(null);
            setSearchResults(res);
        })
        .catch(error => {
            console.log(error.message);
            setErrorMessage(error.message);
            setSearchResults([])
        })
    }

    return (
        <div>
            <h2>Search for Employees</h2>
            <h3>Select search criteria</h3>

            {/* Search input */}
            <FormControl>
                <TextField 
                    label="Employee ID"
                    onChange={employeeIdChanged}
                    value={employeeId}
                />
                <TextField
                    label="Name"
                    onChange={employeeNameChanged}
                    value={employeeName}
                />
                <TextField
                    label="Type"
                    onChange={employeeTypeChanged}
                    value={employeeType}
                />
                <TextField
                    label="Phone number"
                    onChange={employeePhoneChanged}
                    value={employeePhone}
                />
                <br />
                <Button 
                    onClick={searchClick}
                    variant="outlined">
                    Search
                </Button>
            </FormControl>

            {/* Search Results */}
            {errorMessage && 
                <Alert severity="error">
                    {errorMessage}
                </Alert>
            }
            {searchResults.map(employee => {
                return <Employee key={employee.eid} employee={employee} />
            })}
        </div>
    )
}

export default Search;