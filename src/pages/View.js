import react, { useState } from 'react'
import { TextField, Input, Button } from "@material-ui/core"
import { Alert } from "@material-ui/lab"

import Employee from "../components/Employee"
import EmployeeApi from "../APIs/EmployeeApi"

const View = (props) => {

    const [employeeId, setEmployeeId] = useState('');
    const [errorMessage, setErrorMessage] = useState(null);

    // State variable to hold the information of an employee
    const [employee, setEmployee] = useState(null);

    /* Get the employee through the API and change state */
    const getEmployeeClick = () => {

        EmployeeApi.getEmployee(employeeId).then((res) => {
                console.log("Got response", res);
                setErrorMessage(null);
                setEmployee(res);
            })
            .catch(error => {
                console.log(error.message);
                setErrorMessage(error.message);
            })

    }

    const employeeIdChanged = (event) => {
        setEmployeeId(event.target.value);
    }

    return (
        <div>
            <h2>View details of a Registered Employee</h2>
            <TextField required value={employeeId} onChange={employeeIdChanged} label="Employee ID" />
            <br /> <br />
            <Button
                variant="outlined"
                onClick={getEmployeeClick}>
                Get Employee
            </Button>

            <br /> <br />
            {errorMessage ?
                <Alert severity="error">
                    {errorMessage}                
                </Alert>
                : (employee && <Employee employee={employee} />)}
        </div>
    )
}

export default View;