import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import NavigationBar from "./components/NavigationBar.js"
import Delete from "./pages/Delete.js"
import Register from "./pages/Register.js"
import Search from "./pages/Search.js"
import Update from "./pages/Update.js"
import View from "./pages/View.js"

import NavigationBarButton from "./components/NavigationBarButton.js"


function App() {
  return (
    <Router>
      <div className="App">
        <NavigationBar />  
        <Switch>
          <Route exact path="/register">
            <Register />
          </Route>

          <Route exact path="/view">
            <View />
          </Route>

          <Route exact path="/delete">
            <Delete />
          </Route>

          <Route exact path="/update">
            <Update />
          </Route>

          <Route exact path="/search">
            <Search />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
